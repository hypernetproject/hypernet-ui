import Chart from 'chart.js'
import { generateChart } from 'vue-chartjs'

Chart.defaults.global.defaultFontColor = 'grey'
Chart.defaults.global.defaultFontFamily = "Tahoma"
Chart.defaults.global.defaultFontSize = 11
Chart.defaults.global.defaultFontStyle = 'normal'

Chart.defaults.LineWithLine = Chart.defaults.line
Chart.controllers.LineWithLine = Chart.controllers.line.extend({
  draw: function (ease) {
    Chart.controllers.line.prototype.draw.call(this, ease)

    this.chart.options.backgroundColor = 'red'

    let scale = this.chart.scales['x-axis-0']
    // calculate the portion of the axis and multiply by total axis width
    let left = (50 / scale.end * (scale.right - scale.left))

    let ctx = this.chart.ctx
    let x = 200
    let topY = this.chart.scales['y-axis-0'].top
    let bottomY = this.chart.scales['y-axis-0'].bottom

    // Draw line
    // TODO: Make it move on the x-axis such that the line corresponds to the date of today on the graph
    ctx.save()
    ctx.beginPath()
    ctx.moveTo(x, topY + 45)
    ctx.lineTo(x, bottomY)
    ctx.lineWidth = 2
    ctx.strokeStyle = '#07C'
    ctx.stroke()

    // Label of TODAY
    ctx.textAlign = 'center'
    ctx.fillStyle = 'rgba(0, 0, 0, 1)'
    ctx.fillText('TODAY', x, topY - 15)
    ctx.restore()
  }
})

const LineWithLine = generateChart('line-with-chart', 'LineWithLine')

export default {
  extends: LineWithLine,
  mounted () {
    this.renderChart({
      labels: ['May 1', 'June 1'],
      datasets: [
        {
          label: 'Market Cap Value',
          data: [10, 0.1],
          backgroundColor: 'rgba(0,0,0,0.1)'
        }
      ]
    }, {
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        intersect: false
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function (value, index, values) {
              return value + 'M ETH'
            },
            gridLines: {
              color: '#ffffff'
            }
          }
        }],
        xAxes: [{gridLines: { color: 'rgba(0,0,0,0)' }}]
      }
    })
  }
}
