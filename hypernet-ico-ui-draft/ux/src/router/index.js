import Vue from 'vue'
import Router from 'vue-router'
import SalePage from '@/components/SalePage'
import ClaimWithEthereum from '@/components/ClaimWithEthereum'
import ClaimWithBitcoin from '@/components/ClaimWithBitcoin'
import Verify from '@/components/Verify'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SalePage',
      component: SalePage
    },
    {
      path: '/claimWithEthereum',
      name: 'claimWithEthereum',
      component: ClaimWithEthereum
    },
    {
      path: '/claimWithBitcoin',
      name: 'claimWithBitcoin',
      component: ClaimWithBitcoin
    },
    {
      path: '/verify',
      name: 'verify',
      component: Verify
    }
  ]
})
